import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CardInfoComponent } from './dashboard/card-info/card-info.component';
import { CyptoInfoComponent } from './dashboard/cypto-info/cypto-info.component';
import { EWalletComponent } from './e-wallet/e-wallet.component';
import { AffiliatesComponent } from './affiliates/affiliates.component';
import { EventsComponent } from './events/events.component';
import { PaymentsComponent } from './payments/payments.component';
import { ManageMyBusinessComponent } from './manage-my-business/manage-my-business.component';
import { SettingsComponent } from './settings/settings.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    CardInfoComponent,
    CyptoInfoComponent,
    EWalletComponent,
    AffiliatesComponent,
    EventsComponent,
    PaymentsComponent,
    ManageMyBusinessComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PerfectScrollbarModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
