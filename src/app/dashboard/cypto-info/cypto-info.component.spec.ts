import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CyptoInfoComponent } from './cypto-info.component';

describe('CyptoInfoComponent', () => {
  let component: CyptoInfoComponent;
  let fixture: ComponentFixture<CyptoInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CyptoInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CyptoInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
