import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { EWalletComponent } from './e-wallet/e-wallet.component';
import { AffiliatesComponent } from './affiliates/affiliates.component';
import { EventsComponent } from './events/events.component';
import { PaymentsComponent } from './payments/payments.component';
import { ManageMyBusinessComponent } from './manage-my-business/manage-my-business.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'e-wallet', component: EWalletComponent },
  { path: 'affiliates', component: AffiliatesComponent },
  { path: 'events', component: EventsComponent },
  { path: 'payments', component: PaymentsComponent },
  { path: 'manage-my-business', component: ManageMyBusinessComponent },
  { path: 'settings', component: SettingsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
