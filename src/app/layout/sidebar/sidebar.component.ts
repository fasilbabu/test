import { Component, NgZone, OnInit } from '@angular/core';
const smallWidthBreakPoint = 767;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  isActive: boolean = false;

  private mediaMatcher: MediaQueryList = matchMedia(
    `(max-width: ${smallWidthBreakPoint}px)`
  );

  constructor(zone: NgZone) {
    this.mediaMatcher.addListener(() => zone.run(() => this.mediaMatcher));
  }

  ngOnInit(): void {}

  isScreenSmall() {
    if (this.mediaMatcher.matches) this.isActive = false;
    return this.mediaMatcher.matches;
  }
}
